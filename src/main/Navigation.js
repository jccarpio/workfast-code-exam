import { COLOR_BACKGROUND, COLOR_LIGHT_TEXT, COLOR_SECONDARY } from '../common/themeColors'
import React, { Component } from 'react'
import styled from 'styled-components'

const StyledNavigation = styled.nav`
  background-color: ${COLOR_SECONDARY};
  .nav {
    margin: 0;
    padding: 0;
    list-style: none;
    float: right;
    li {
      display: inline-block;
      a {
        transition: all 0.3s ease;
        display: block;
        height: 55px;
        line-height: 55px;
        box-sizing: border-box;
        margin: 0 25px;
        text-transform: uppercase;
        text-decoration: none;
        color: ${COLOR_BACKGROUND};
      }
      &:last-of-type {
        a {
          margin-right: 0;
        }
      }
      &.active,
      &:hover {
        a {
          font-weight: 500;
          border-bottom: 3px solid ${COLOR_BACKGROUND};
        }
      }
    }
  }
`

export default class Navigation extends Component {
  static NAV_ITEMS = ['Industries', 'Employer', 'Projects', 'Worker', 'Jobs', 'How it works']

  render() {
    return (
      <StyledNavigation className="group">
        <div className="width-container">
          <ul className="nav group">
            {Navigation.NAV_ITEMS.map((item, index) => {
              return (
                <li key={index} className={index === 0 ? 'active' : ''}>
                  <a href="#">{item}</a>
                </li>
              )
            })}
          </ul>
        </div>
      </StyledNavigation>
    )
  }
}
