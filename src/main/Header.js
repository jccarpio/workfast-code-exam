import { Button } from '../common'
import logo from '../images/Logo.png'
import React, { Component } from 'react'
import styled from 'styled-components'

const StyledHeader = styled.section`
  padding: 25px 0;
  .logo-link,
  .header-utils {
    height: 37px;
    line-height: 37px;
  }
  .logo-link {
    display: inline-block;
    line-height: 37px;
    img {
      height: 33px;
    }
  }
  .header-utils {
    p {
      margin: 0;
    }
    b {
      display: inline-block;
      padding: 0 10px;
      font-weight: 700;
    }
  }
`

export default class Header extends Component {
  render() {
    return (
      <StyledHeader className="group">
        <div className="width-container">
          <div className="group">
            <div className="brand-container left">
              <a href="#" className="logo-link">
                <img src={logo} />
              </a>
            </div>
            <div className="header-utils right">
              <p>
                <span>Need staff ?</span>
                <b>CALL 1300 824 403</b>
                <Button
                  theme="secondary"
                  mleft="10px"
                  padding="padding"
                  dimensions={{ width: '90px', height: '37px' }}
                  onClick={e => alert('test')}
                  href="#"
                >
                  Login
                </Button>
              </p>
            </div>
          </div>
        </div>
      </StyledHeader>
    )
  }
}
