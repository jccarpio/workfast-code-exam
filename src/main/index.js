import Header from './Header'
import Home from '../home'
import Navigation from './Navigation'
import React, { Component } from 'react'

export default class MainScreen extends Component {
  render() {
    return (
      <React.Fragment>
        <Header />
        <Navigation />
        <div className="page">
          <Home />
        </div>
      </React.Fragment>
    )
  }
}
