import './css/app.css'
import './css/normalize.css'
import 'font-awesome/css/font-awesome.min.css'
import Main from './main'
import React from 'react'
import ReactDOM from 'react-dom'

if (document.getElementById('app')) {
  ReactDOM.render(<Main />, document.getElementById('app'))
}
