import { Button } from '../common'
import { COLOR_BACKGROUND, COLOR_BACKGROUND_SECONDARY } from '../common/themeColors'
import banner from '../images/Banner.jpg'
import CivilIcon from '../images/icon-civil.png'
import ConstructionIcon from '../images/icon-construction.png'
import LabourIcon from '../images/icon-labour.png'
import MiningIcon from '../images/icon-mining.png'
import OfficeIcon from '../images/icon-office.png'
import React, { Component } from 'react'
import styled from 'styled-components'
import WarehousingIcon from '../images/icon-warehousing.png'

const StyledBanner = styled.section`
  .hero-container {
    background-image: url(${banner});
    color: ${COLOR_BACKGROUND};
    background-size: cover;
    background-position: center;
  }
  .banner-overlay {
    padding: 102px 0;
    box-sizing: border-box;
    min-height: 350px;
    background-color: rgba(0, 0, 0, 0.23);
  }
  .btn-text {
    font-weight: 500;
  }
  .heading-container {
    padding: 0 25%;
  }
  h2 {
    font-size: 43px;
    line-height: 60px;
    font-weight: 600;
    margin: 15px 0 30px 0;
  }
  p {
    font-size: 19px;
  }
  .industries-container {
    background: ${COLOR_BACKGROUND_SECONDARY};
    padding: 50px 0;
    .icon {
      width: 40px;
      height: 50px;
      background-size: contain;
      background-position: center;
      background-repeat: no-repeat;
      margin-right: 5px;
    }
    .name {
      line-height: 40px;
    }
    .industry-item {
      text-align: center;
      box-sizing: border-box;
      .name,
      .icon {
        display: inline-block;
        vertical-align: middle;
      }
    }
  }
`

export default class Banner extends Component {
  static INDUSTRIES = [
    {
      name: 'Civil and rail',
      icon: CivilIcon,
    },
    {
      name: 'Mining',
      icon: MiningIcon,
    },
    {
      name: 'Construction',
      icon: ConstructionIcon,
    },
    {
      name: 'Warehousing',
      icon: WarehousingIcon,
    },
    {
      name: 'Labour hire',
      icon: LabourIcon,
    },
    {
      name: 'Office admin',
      icon: OfficeIcon,
    },
  ]

  render() {
    return (
      <StyledBanner className="group tcenter">
        <section className="hero-container">
          <div className="banner-overlay">
            <div className="heading-container">
              <h2>
                Need Managed Labour Hire<br />or Contract Workers?
              </h2>
              <p>1 to 200+ workers simply Phone: 1300 824 403</p>
              <Button padding="16px 70px" mtop="50px" href="#">
                <span className="btn-text">Hire Staff</span>
              </Button>
            </div>
          </div>
        </section>
        <section className="industries-container">
          <div className="width-container">
            <section className="group">{this.renderIndustries()}</section>
          </div>
        </section>
      </StyledBanner>
    )
  }

  renderIndustries() {
    return Banner.INDUSTRIES.map((industry, index) => {
      return (
        <div key={index} className="industry-item left group" style={{ width: `${100 / Banner.INDUSTRIES.length}%` }}>
          <div className="icon" style={{ backgroundImage: `url(${industry.icon})` }} />
          <span className="name">{industry.name}</span>
        </div>
      )
    })
  }
}
