import { COLOR_BACKGROUND_SECONDARY } from '../common/themeColors'
import { SectionHeader } from '../common'
import ACTLogo from '../images/act-goverment.png'
import BMDLogo from '../images/bmd.png'
import DegnanLogo from '../images/degnan.png'
import DownerLogo from '../images/downer.png'
import HarveyNormanLogo from '../images/harvey-norman.png'
import HiltiLogo from '../images/hilti.png'
import KoneLogo from '../images/kone.png'
import LendleaseLogo from '../images/lendlease.png'
import NSWLogo from '../images/nsw.png'
import React, { Component } from 'react'
import RRALogo from '../images/rra.png'
import styled from 'styled-components'
import UGLLogo from '../images/ugl.png'
import UMSLogo from '../images/ums.png'

const StyledPartners = styled.section`
  border-bottom: 2px solid ${COLOR_BACKGROUND_SECONDARY};
  padding: 10px 30px;
  .partner-container {
    width: 16%;
    height: 70px;
    background: none center center no-repeat;
    float: left;
    text-align: center;
    background-size: contain;
    margin: 15px 0;
    img {
      display: inline-block;
      height: 100px;
    }
  }
  .partner-logo-container {
    display: inline-block;
    height: 100px;
    line-height: 100px;
    margin: 15px 0;
    width: 16%;
    padding: 0 20px;
    text-align: center;
    box-sizing: border-box;
  }
  .partner-logo {
    max-height: 95px;
    max-width: 100%;
    display: inline-block;
    text-align: center;
  }
`

export default class TrustedPartners extends Component {
  static PARTNERS = [
    {
      name: 'Rhomberg Rail Australia',
      image: RRALogo,
    },
    {
      name: 'Downer',
      image: DownerLogo,
    },
    {
      name: 'Hilti',
      image: HiltiLogo,
    },
    {
      name: 'Harvey Normal',
      image: HarveyNormanLogo,
    },
    {
      name: 'Lendlease',
      image: LendleaseLogo,
    },
    {
      name: 'Urban Maintenance Systems',
      image: UMSLogo,
    },
    {
      name: 'Degnan',
      image: DegnanLogo,
    },
    {
      name: 'KONE',
      image: KoneLogo,
    },
    {
      name: 'BMD',
      image: BMDLogo,
    },
    {
      name: 'NSW',
      image: NSWLogo,
    },
    {
      name: 'UGL',
      image: UGLLogo,
    },
    {
      name: 'ACT Goverment',
      image: ACTLogo,
    },
  ]

  render() {
    return (
      <StyledPartners>
        <SectionHeader>Our trusted partners</SectionHeader>
        <p>We work with the most trusted companies across Australia - 600+ companies!</p>
        <div className="group">{this.renderPartnersLogos()}</div>
      </StyledPartners>
    )
  }

  renderPartnersLogos() {
    return TrustedPartners.PARTNERS.map((partner, index) => {
      return (
        <div className="partner-logo-container" key={index}>
          <img className="partner-logo" src={partner.image} alt={partner.name} />
        </div>
      )
    })
  }
}
