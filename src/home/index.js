import Banner from './Banner'
import FeaturedProjects from './FeaturedProjects'
import React, { Component } from 'react'
import TrustedPartners from './TrustedPartners'

export default class Home extends Component {
  render() {
    return (
      <section>
        <Banner />
        <div className="width-container">
          <TrustedPartners />
          <FeaturedProjects />
        </div>
      </section>
    )
  }
}
