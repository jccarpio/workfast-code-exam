import { Button } from '../common'
import { COLOR_BACKGROUND, COLOR_LIGHT_TEXT } from '../common/themeColors'
import { SectionHeader } from '../common'
import NewcastleLightRail from '../images/newcastle-light-rail-project.jpg'
import React, { Component } from 'react'
import RichmondStation from '../images/richmond-station.jpg'
import RoyalMelbourne from '../images/royal-melbourne-hospital.png'
import SeventhStreetExt from '../images/sevent-street-extension.jpg'
import styled from 'styled-components'

const StyledProjects = styled.section`
  padding-bottom: 20px;
  .btn-text,
  .btn {
    font-weight: 500;
  }
  .heading-container {
    margin: 0 30px;
    padding: 30px 0;
  }
  .project {
    height: 300px;
    background: none center center no-repeat;
    background-size: cover;
    float: left;
    position: relative;
    margin-bottom: 25px;
    .label {
      transition: all 0.3s ease;
      position: absolute;
      top: auto;
      bottom: 0;
      left: 0;
      right: 0;
      padding: 40px 25px;
      background: -moz-linear-gradient(top, rgba(255, 255, 255, 0) 0%, rgba(0, 0, 0, 1) 100%);
      background: -webkit-linear-gradient(top, rgba(255, 255, 255, 0) 0%, rgba(0, 0, 0, 1) 100%);
      background: linear-gradient(to bottom, rgba(255, 255, 255, 0) 0%, rgba(0, 0, 0, 1) 100%);
      filter: progid:DXImageTransform.Microsoft.gradient(
          startColorstr='#00ffffff',
          endColorstr='#000000',
          GradientType=0
        );
      .title {
        margin: 10px 0;
        font-weight: 500;
        font-size: 18px;
        color: ${COLOR_BACKGROUND};
      }
      .location {
        color: ${COLOR_LIGHT_TEXT};
        text-transform: uppercase;
      }
      .btn {
        display: none;
        font-weight: 500;
        font-size: 17px;
        padding: 15px;
        margin: 40px auto 0;
        width: 140px;
        color: ${COLOR_BACKGROUND};
        border: 2px solid ${COLOR_BACKGROUND};
        text-decoration: none;
      }
    }
    &:hover {
      .label {
        top: 0;
        background: rgba(0, 0, 0, 0.6);
        & > div {
          position: absolute;
          top: 50%;
          transform: translateY(-50%);
          margin-left: -25px;
          text-align: center;
          width: 100%;
        }
        .title {
          font-size: 24px;
          margin-bottom: 20px;
        }
        .location {
          font-size: 16px;
          color: ${COLOR_BACKGROUND};
        }
        .btn {
          display: block;
        }
      }
    }
  }
`

export default class FeaturedProjects extends Component {
  static PROJECTS = [
    {
      name: 'Seventh Street Extension Sydney Airport',
      location: 'Sydney, NSW',
      image: SeventhStreetExt,
      isLarge: true,
    },
    {
      name: 'Richmond Station Upgrade',
      location: 'Sydney, NSW',
      image: RichmondStation,
    },
    {
      name: 'Royal Melbourne Hospital Upgrade',
      location: 'Melbourne, VIC',
      image: RoyalMelbourne,
    },
    {
      name: 'Newcastle Light Rail Project',
      location: 'Newcastle, NSW',
      image: NewcastleLightRail,
      isLarge: true,
    },
  ]

  render() {
    return (
      <StyledProjects>
        <div className="heading-container">
          <SectionHeader>Featured Projects</SectionHeader>
        </div>
        <section className="group projects-container">{this.renderProjects()}</section>
        <div className="tcenter">
          <Button padding="15px 70px" mtop="40px" href="#">
            <span className="btn-text">All Projects</span>
          </Button>
        </div>
      </StyledProjects>
    )
  }

  renderProjects() {
    return FeaturedProjects.PROJECTS.map((project, index) => {
      return (
        <div
          className="project"
          key={index}
          style={{
            backgroundImage: `url(${project.image})`,
            width: project.isLarge ? '55%' : '40%',
            marginRight: index % 2 === 0 ? '25px' : 0,
          }}
        >
          <div className="label">
            <div>
              <h3 className="title">{project.name}</h3>
              <span className="location">{project.location}</span>
              <a href="#" className="btn">
                See Project
              </a>
            </div>
          </div>
        </div>
      )
    })
  }
}
