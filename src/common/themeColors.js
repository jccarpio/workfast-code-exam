export const COLOR_PRIMARY = '#2ebee9'
export const COLOR_SECONDARY = '#333'

export const COLOR_BACKGROUND = '#fff'
export const COLOR_BACKGROUND_SECONDARY = '#eef4f5'

export const COLOR_SECONDARY_TEXT = '#484848'
export const COLOR_LIGHT_TEXT = '#c9c9c9'
