import { COLOR_BACKGROUND, COLOR_PRIMARY, COLOR_SECONDARY } from './themeColors'
import styled, { css } from 'styled-components'

const getBackgroundColorStyle = props => {
  let backgroundColor = COLOR_PRIMARY
  const color = COLOR_BACKGROUND
  const { theme } = props
  if (theme === 'primary') {
    backgroundColor = COLOR_PRIMARY
  }
  if (theme === 'secondary') {
    backgroundColor = COLOR_SECONDARY
  }
  return css`
     {
      background-color: ${backgroundColor};
      color: ${color};
      &: hover {
        background-color: ${backgroundColor};
        color: ${color};
      }
    }
  `
}

const getPaddingStyle = props => {
  const padding = '5px 10px'
  const { padding: paddingProps } = props
  return css`
     {
      padding: ${paddingProps || padding};
    }
  `
}

const getDimensionsStyle = props => {
  if (!props.dimensions) {
    return null
  }
  const { width = 'auto', height = 'auto' } = props.dimensions
  return css`
     {
      width: ${width};
      height: ${height};
      line-height: ${height};
    }
  `
}

export default styled.a`
  border: none;
  display: inline-block;
  text-decoration: none;
  text-align: center;
  margin-top: ${props => props.mtop && `${props.mtop}`};
  margin-right: ${props => props.mright && `${props.mright}`};
  margin-bottom: ${props => props.mbottom && `${props.mbottom}`};
  margin-left: ${props => props.mleft && `${props.mleft}`};
  ${props => getDimensionsStyle(props)} ${props => getBackgroundColorStyle(props)};
  ${props => getPaddingStyle(props)};
`
