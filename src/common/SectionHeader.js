import { COLOR_SECONDARY } from './themeColors'
import arrows from '../images/heading-arrows.png'
import styled from 'styled-components'

export default styled.h3`
  text-transform: uppercase;
  color: ${COLOR_SECONDARY};
  background: url(${arrows}) left center no-repeat;
  line-height: 22px;
  padding-left: 50px;
  font-size: 23px;
  margin: 35px 0;
`
