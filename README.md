## Workfast website code exam

#### After cloning
1. run `yarn install --pure-lockfile`

#### Running Locally
1. `yarn start`
2. Go to http://localhost:3000

#### Technologies Used
- `react`
- `styled-components`
- `eslint`
- `prettier`
- `webpack`
